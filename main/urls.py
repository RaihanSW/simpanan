from django.urls import path
from . import views

urlpatterns = [
    path('', views.home,name="home"),
    path('hello', views.index2,name="index"),
    path('<str:name>', views.input,name="input"),
]
