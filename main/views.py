from django.shortcuts import render
from django.http import HttpResponse
from .models import ToDolist, Item
# Create your views here.


def index(response):
    return HttpResponse("<h1>Homepage</h1>")

def index2(response):
    return HttpResponse("<h1>Hello World</h1>")

def input(response,name):
    ls = ToDolist.objects.get(name=name)
    items = ls.item_set.get(id=1)
    return HttpResponse("<h1>%s</h1><br><p>%s</p>" %(ls.name, str(items.text)))

def home(response):
    pass